import Ember from 'ember';
import Resolver from './brand.resolver';
import loadInitializers from 'ember/load-initializers';
import config from './config/environment';
import brandConfig from './brands/brands-config';

Ember.MODEL_FACTORY_INJECTIONS = true;

var brandFalbacChain = brandConfig[config.APP.brand] || ['default'];

var App = Ember.Application.extend({
    modulePrefix: config.modulePrefix,
    podModulePrefix: config.podModulePrefix,
	brandFallbackChain: brandFalbacChain,
    LOG_VIEW_LOOKUPS: true,
    LOG_RESOLVER: true,
    Resolver: Resolver
});

loadInitializers(App, config.modulePrefix);
brandFalbacChain.reverse().forEach(function(brand) {
	loadInitializers(App, config.modulePrefix + '/brands/' + brand);
});
// Reverse array again to return it to the order
brandFalbacChain.reverse();

export default App;
