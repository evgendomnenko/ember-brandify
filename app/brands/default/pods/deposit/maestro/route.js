// import Router from 'router';
import DepositMethodRoute from '../../deposit/credit-card/route';
//
// // Router.map(function () {
// //     this.resource('deposit', function () {
// //         this.route('visa');
// //     });
// // });
// //
export default DepositMethodRoute.extend({ 
    setupController: function(controller, model) {
        this._super(controller, model);
        controller.set('title', 'Maestro Deposit');
    }
});

