 import Ember from "ember";

 export default Ember.Route.extend({
    actions: {
        didTransition: function() {
            var lastPaymentMethodRoute = this.get('controller').get('lastPaymentMethodRoute');
            this.transitionTo(lastPaymentMethodRoute || 'deposit.visa');
            return true;
        }
    }
});

