import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
    this.resource('deposit', function() {
        this.route('visa');
        this.route('ecmc');
        this.route('maestro');
    });

    this.resource('withdraw');
});

export default Router;
