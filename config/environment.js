/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'ember-brandify',
    podModulePrefix: 'ember-brandify/pods',
    environment: environment,
    baseURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };
  
  var envSplit = environment.split(':');
  var brand;
  if(envSplit.length === 1) {
	switch(environment) {
	case 'development':
	case 'test':
	case 'production':
		brand = 'default'
		break;
	default:
		brand = environment;
		environment = 'development'
		break;
	}
  } else {
	brand = envSplit[0];
	environment = envSplit[1];
  }
  
  ENV.APP.brand = brand;

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'auto';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {

  }

  return ENV;
};
